package org.axe.jetty.config;

import java.util.List;

import org.axe.jetty.constant.ConfigConstant;
import org.axe.util.AxeProperties;

public class JettyConfig {

	private int serverPort;
	
	private List<JettyServletFilter> filterList;

	private String sslKeyPath;
	
	private String sslPassword;
	
	public JettyConfig() {
		this.serverPort = AxeProperties.getInt(ConfigConstant.AXE_PORT);
		this.sslKeyPath = AxeProperties.getString(ConfigConstant.AXE_SSL_KEY_PATH,null);
		this.sslPassword = AxeProperties.getString(ConfigConstant.AXE_SSL_KEY_PASSWORD,null);
	}

	public JettyConfig(int serverPort) {
		this.serverPort = serverPort;
		this.sslKeyPath = AxeProperties.getString(ConfigConstant.AXE_SSL_KEY_PATH,null);
		this.sslPassword = AxeProperties.getString(ConfigConstant.AXE_SSL_KEY_PASSWORD,null);
	}

	public int getServerPort() {
		return serverPort;
	}

	public void setServerPort(int serverPort) {
		this.serverPort = serverPort;
	}

	public List<JettyServletFilter> getFilterList() {
		return filterList;
	}

	public void setFilterList(List<JettyServletFilter> filterList) {
		this.filterList = filterList;
	}

	public String getSslKeyPath() {
		return sslKeyPath;
	}

	public void setSslKeyPath(String sslKeyPath) {
		this.sslKeyPath = sslKeyPath;
	}

	public String getSslPassword() {
		return sslPassword;
	}

	public void setSslPassword(String sslPassword) {
		this.sslPassword = sslPassword;
	}
	
	
}
