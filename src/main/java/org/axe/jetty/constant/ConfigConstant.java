package org.axe.jetty.constant;

public final class ConfigConstant {
	
	public static final String AXE_PORT = "axe.port";
	public static final String AXE_CONTEXT_PATH = "axe.contextPath";
	public static final String AXE_SSL_KEY_PATH = "axe.sslKeyPath";
	public static final String AXE_SSL_KEY_PASSWORD = "axe.sslPassword";
	
	
}
